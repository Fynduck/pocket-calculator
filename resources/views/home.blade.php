@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card text-center">
                <h1 class="my-4">Welcome <strong>{{ auth()->user()->name }}</strong></h1>
            </div>
        </div>
    </div>
</div>
@endsection
