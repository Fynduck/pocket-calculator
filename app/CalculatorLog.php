<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalculatorLog extends Model
{
    protected $fillable = [
        'user_id',
        'text',
        'total'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
