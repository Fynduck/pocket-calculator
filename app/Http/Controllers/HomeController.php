<?php

namespace App\Http\Controllers;

use App\CalculatorLog;
use App\Http\Resources\CalculatorLogResource;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['calculate', 'getUserLog']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function calculate(Request $request)
    {
        $calculate = $this->replaceMethodsOfFinish($request->get('calculate'));
        $total = null;
        eval('$total =' . $calculate . ';');

        $response = [
            'histories' => [
                'text'  => $calculate,
                'total' => $total
            ]
        ];
        if (auth()->check()) {
            $log = CalculatorLog::create([
                'user_id' => auth()->id(),
                'text'    => $calculate,
                'total'   => $total
            ]);

            $response = [
                'histories' => new CalculatorLogResource($log)
            ];
        }

        return response()->json($response);
    }

    private function replaceMethodsOfFinish($calculate)
    {
        $last = substr($calculate, -1);
        if (!is_numeric($last)) {
            $calculate = substr($calculate, 0, -1);

            return $this->replaceMethodsOfFinish($calculate);
        }

        return $calculate;
    }

    public function getUserLog()
    {
        $logs = [];
        if (auth()->check())
            $logs = CalculatorLogResource::collection(auth()->user()->calculatorLog->sortByDesc('id'));

        return response()->json($logs);
    }
}
