## How start

### Run next commands
- clone repository
- composer install
- cp .env.example .env
- php artisan key:generate
- set in .env credentials data to mysql or create file sqlite
- php artisan migrate
- npm i
- npm run dev